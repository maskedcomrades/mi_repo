﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


//The GameMaster should handle any interactions between objects that don't involve direct collisions.
//This can be things like where the camera will be moving  based on where the characters are or updating the HUD for what's going on in the game.
//This way, we don't have objects being passed around literally everywhere in the level, making references (hopefully) easier to manage

public class GameMaster : MonoBehaviour {

    //player1Char and player2Char are currently public only because there's no real particular good way to set these right now.
    //Later on, we'll probably be instantiating different characters through the GM. At that point, we can set these on initiation.
    public GameObject player1Char;
    public GameObject player2Char;
    private FighterController p1Controller;
    private FighterController p2Controller;
    public Image p1HealthBar;
    public Image p2HealthBar;
    public Text UITimer;
    public float timeLeft = 99;

	// Use this for initialization
	void Start () {
        p1Controller = player1Char.GetComponent<FighterController>();
        p2Controller = player2Char.GetComponent<FighterController>();
    }
	
	// Update is called once per frame
	void Update () {
        timeLeft -= Time.deltaTime;
        UpdateUI();

    }

    void UpdateUI()
    {
        p1HealthBar.fillAmount = p1Controller.GetHealthRatio();
        p2HealthBar.fillAmount = p2Controller.GetHealthRatio();
        UITimer.text = timeLeft.ToString("N0"); //'N' for 'number' and 0 for # of decimal places
    }

}
