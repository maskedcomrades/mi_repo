﻿using UnityEngine;
using System.Collections;

public class MediumBoxes : MonoBehaviour {

    public BoxCollider2D mediumbox;


    void MediumHitboxEnd() {
        mediumbox.size = new Vector2(0.0f, 0.0f);

    }

    #region standing medium
    void MediumHitboxStart(int test) {
        mediumbox.offset = new Vector2(2.761152f, 0.3955027f);
        mediumbox.size = new Vector2(3.100838f, 1.942256f);

    }

    void MediumHitboxFrame2() {
        mediumbox.offset = new Vector2(2.926682f, 0.9200363f);
        mediumbox.size = new Vector2(2.926682f, 1.363495f);

    }

    void MediumHitboxFrame3() {
        mediumbox.offset = new Vector2(2.595621f, 0.9200363f);
        mediumbox.size = new Vector2(2.769779f, 1.363495f);

    }
    void MediumHitboxFrame4() {
        mediumbox.offset = new Vector2(2.09903f, 0.1472066f);
        mediumbox.size = new Vector2(1.776599f, 1.693958f);

    }

    void MediumHitboxFrame5() {
        mediumbox.offset = new Vector2(1.850739f, -0.01832326f);
        mediumbox.size = new Vector2(1.280015f, 1.693959f);

    }

    #endregion

    #region crouching medium
    void CrouchingMediumHitboxStart() {
        mediumbox.offset = new Vector2(1.974895f, -1.321864f);
        mediumbox.size = new Vector2(1.776622f, 1.487056f);

    }

    void CrouchingMediumHitboxFrame2() {
        mediumbox.offset = new Vector2(3.27844f, 0.002377828f);
        mediumbox.size = new Vector2(3.307785f, 2.894064f);

    }

    void CrouchingMediumHitboxFrame3() {
        mediumbox.offset = new Vector2(3.27844f, 0.002377828f);
        mediumbox.size = new Vector2(3.307785f, 2.894064f);

    }
    void CrouchingMediumHitboxFrame4() {
        mediumbox.offset = new Vector2(3.009454f, -0.3286819f);
        mediumbox.size = new Vector2(2.769812f, 2.231944f);

    }

    void CrouchingMediumHitboxFrame5() {
        mediumbox.offset = new Vector2(2.637006f, -0.84596f);
        mediumbox.size = new Vector2(2.024915f, 1.942271f);

    }

    #endregion

    #region jumping medium
    void JumpingMediumHitboxStart() {
        mediumbox.offset = new Vector2(0.1747131f, -0.8666229f);
        mediumbox.size = new Vector2(8.190864f, 4.797592f);

    }

    void JumpingMediumHitboxFrame2() {
        mediumbox.offset = new Vector2(0.1747131f, -0.8666229f);
        mediumbox.size = new Vector2(8.190864f, 4.797592f);

    }

    void JumpingMediumHitboxFrame3() {
        mediumbox.offset = new Vector2(0.1747131f, -0.8666229f);
        mediumbox.size = new Vector2(8.190864f, 4.797592f);

    }
    void JumpingMediumHitboxFrame4() {
        mediumbox.offset = new Vector2(-0.03219859f, -1.239065f);
        mediumbox.size = new Vector2(7.777038f, 4.052709f);

    }
    #endregion
}