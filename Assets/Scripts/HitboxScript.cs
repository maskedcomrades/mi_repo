﻿using UnityEngine;
using System.Collections;


//  This is meant to be a generic hurtbox script.
//  The intention is for the owning character to modify its values when an attack is chosen.
//  Its size will be determined by the animations set up in the animator
//  and it can handle calling the collision functions to anything it hits
public class HitboxScript : MonoBehaviour {

    public HitboxManager myManager;
    public float damage;
    public int hitstunDuration; //time enemy is stunned for this attack
    public int priority; //higher is better. for instance, 1 is "light" and 3 is "heavy"
                         //This allows us to "overtake" lower priority moves on collision
    public Vector2 knockback; //when the enemy is hit, are they knocked back?

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void FixedUpdate () {
	}

    //Enemies can contain multiple hitboxes. If they do, we don't want to apply damage more than once.
    //Therefore, we'll check if the object we entered is already in the queue
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (myManager.AddCollision(collision)) //if this is a new collision for this move...
        {//...apply damage
            FighterController hitMe = collision.gameObject.GetComponent<FighterController>();
            hitMe.TakeDamage(damage, hitstunDuration, priority, knockback);
        }
    }

}
