﻿using UnityEngine;
using System.Collections;


public class HitboxManager : MonoBehaviour {


    HitboxScript[] hitboxes;
    FrameCounter activeTime; //How long until we can hit the same thing twice?
    public string ownerTag; //Who owns this hurtbox? So we can ignore it
    public float damage;
    public int hitstunDuration; //time enemy is stunned for this attack
    public int priority; //higher is better. for instance, 1 is "light" and 3 is "heavy"
                         //This allows us to "overtake" lower priority moves on collision
    public Vector2 knockback; //when the enemy is hit, are they knocked back?


    public Queue hitQueue = new Queue(); //what things did we hit while this is active?

    // Use this for initialization
    void Start () {
        activeTime = new FrameCounter();
        activeTime.SetCallbackFunction(ClearHitQueue);
        hitboxes = gameObject.GetComponentsInChildren<HitboxScript>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        activeTime.UpdateTimer();
	}

    void ClearHitQueue()
    {
        hitQueue.Clear();
        activeTime.Reset();
    }

    //When a collision is detected, this should be called by the triggered hitbox.
    //If the collision has not happened in the given activated time (hense they would
    //not be in hitQueue), then it is added to the queue and a TRUE value is given,
    //letting the hitbox know it can apply damage.
    //If the collision is not new (and thus is already in hitQueue), FALSE will be
    //returned, letting the hitbox know it shouldn't apply damage
    public bool AddCollision(Collider2D collision)
    {
        Debug.Log("HitSOMETHING! " + collision.tag);
        if (collision.tag != ownerTag)
        {
            GameObject gotHit = collision.gameObject;
            if (!hitQueue.Contains(gotHit))
            {
                Debug.Log("GETHIT!" + collision.tag);
                hitQueue.Enqueue(gotHit);
                return true;
            }
        }
        return false;
    }

    public void UpdateHitboxes(float hitDamage, int hitStunTime, int movePriority, Vector2 hitKnockback)
    {
        foreach (HitboxScript hitbox in hitboxes)
        {
            hitbox.damage = hitDamage;
            hitbox.hitstunDuration = hitStunTime;
            hitbox.priority = movePriority;
            hitbox.knockback = hitKnockback;
        }
    }


    public void ActivateMove(int moveDuration, bool multihit = false)
    {
        activeTime.SetEndFrame(moveDuration);
        activeTime.Reset(false); //reset and start
        activeTime.looping = multihit; //timer needs to loop if the move hits multiple times
    }
}
