﻿using UnityEngine;
using System.Collections;

public class KickBoxes : MonoBehaviour {

    public BoxCollider2D kickbox;

    void KickHitboxEnd() {
        kickbox.size = new Vector2(0.0f, 0.0f);

    }

    #region standing kick
    void KickHitboxStart() {
        kickbox.offset = new Vector2(2.637006f, -1.777089f);
        kickbox.size = new Vector2(2.852544f, 2.231908f);

    }

    void KickHitboxFrame2() {
        kickbox.offset = new Vector2(2.823228f, -1.777089f);
        kickbox.size = new Vector2(3.255606f, 2.231908f);

    }

    void KickHitboxFrame3() {
        kickbox.offset = new Vector2(3.224987f, -1.90111f);
        kickbox.size = new Vector2(3.007568f, 1.983866f);

    }

    #endregion

    #region crouching kick
    void CrouchingKickHitboxStart() {
        kickbox.offset = new Vector2(2.409401f, -2.166639f);
        kickbox.size = new Vector2(3.225008f, 1.233762f);

    }

    void CrouchingKickHitboxFrame2() {
        kickbox.offset = new Vector2(2.409401f, -2.166639f);
        kickbox.size = new Vector2(3.225008f, 1.233762f);

    }

    void CrouchingKickHitboxFrame3() {
        kickbox.offset = new Vector2(2.409401f, -2.166639f);
        kickbox.size = new Vector2(3.225008f, 1.233762f);

    }

    void CrouchingKickHitboxFrame4() {
        kickbox.offset = new Vector2(1.928694f, -2.166639f);
        kickbox.size = new Vector2(2.263595f, 1.233762f);

    }

    #endregion

    #region jumping kick
    void JumpingKickHitboxStart() {
        kickbox.offset = new Vector2(1.043734f, -0.6390178f);
        kickbox.size = new Vector2(3.556047f, 3.928568f);

    }

    void JumpingKickHitboxFrame2() {
        kickbox.offset = new Vector2(1.043734f, -0.6390178f);
        kickbox.size = new Vector2(3.556047f, 3.928568f);

    }

    void JumpingKickHitboxFrame3() {
        kickbox.offset = new Vector2(1.043734f, -0.6390178f);
        kickbox.size = new Vector2(3.556047f, 3.928568f);

    }

    void JumpingKickHitboxFrame4() {
        kickbox.offset = new Vector2(1.043734f, -0.6390178f);
        kickbox.size = new Vector2(3.556047f, 3.928568f);

    }
    #endregion
}