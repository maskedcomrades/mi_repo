﻿using UnityEngine;
using System.Collections;

public class HeavyBoxes : MonoBehaviour {

    public BoxCollider2D heavybox;

    void HeavyHitboxEnd() {
        heavybox.size = new Vector2(0.0f, 0.0f);

    }

    #region standing heavy
    void HeavyHitboxStart() {
        heavybox.offset = new Vector2(2.347337f, 0.6231105f);
        heavybox.size = new Vector2(2.27321f, 1.818098f);

    }

    void HeavyHitboxFrame2() {
        heavybox.offset = new Vector2(2.823228f, 0.9127859f);
        heavybox.size = new Vector2(2.397355f, 1.983628f);

    }

    void HeavyHitboxFrame3() {
        heavybox.offset = new Vector2(2.202499f, 3.685407f);
        heavybox.size = new Vector2(2.231828f, 4.218282f);

    }

    void HeavyHitboxFrame4() {
        heavybox.offset = new Vector2(2.202499f, 3.685407f);
        heavybox.size = new Vector2(2.231828f, 4.218282f);

    }

    void HeavyHitboxFrame5() {
        heavybox.offset = new Vector2(2.202499f, 3.685407f);
        heavybox.size = new Vector2(2.231828f, 4.218282f);

    }

    void HeavyHitboxFrame6() {
        heavybox.offset = new Vector2(2.202499f, 3.685407f);
        heavybox.size = new Vector2(2.231828f, 4.218282f);

    }

    void HeavyHitboxFrame7() {
        heavybox.offset = new Vector2(2.34734f, 2.91983f);
        heavybox.size = new Vector2(2.52151f, 2.687129f);

    }

    void HeavyHitboxFrame8() {
        heavybox.offset = new Vector2(2.34734f, 2.63015f);
        heavybox.size = new Vector2(2.52151f, 2.107768f);

    }
    #endregion

    #region crouching heavy
    void CrouchingHeavyHitboxStart() {
        heavybox.offset = new Vector2(3.650858f, -2.094533f);
        heavybox.size = new Vector2(5.376869f, 1.294372f);

    }

    void CrouchingHeavyHitboxFrame2() {
        heavybox.offset = new Vector2(3.650858f, -2.094533f);
        heavybox.size = new Vector2(5.376869f, 1.294372f);

    }

    void CrouchingHeavyHitboxFrame3() {
        heavybox.offset = new Vector2(3.650858f, -1.767047f);
        heavybox.size = new Vector2(5.376869f, 1.949338f);

    }

    void CrouchingHeavyHitboxFrame4() {
        heavybox.offset = new Vector2(3.650858f, -1.767047f);
        heavybox.size = new Vector2(5.376869f, 1.949338f);

    }

    void CrouchingHeavyHitboxFrame5() {
        heavybox.offset = new Vector2(3.650858f, -1.767047f);
        heavybox.size = new Vector2(5.376869f, 1.949338f);

    }

    void CrouchingHeavyHitboxFrame6() {
        heavybox.offset = new Vector2(3.650858f, -1.767047f);
        heavybox.size = new Vector2(5.376869f, 1.949338f);

    }

    void CrouchingHeavyHitboxFrame7() {
        heavybox.offset = new Vector2(3.014609f, -1.767047f);
        heavybox.size = new Vector2(4.104373f, 1.949338f);

    }
    #endregion

    #region jumping heavy
    void JumpingHeavyHitboxStart() {
        heavybox.offset = new Vector2(1.892062f, -0.1217397f);
        heavybox.size = new Vector2(5.252706f, 4.96313f);

    }

    void JumpingHeavyHitboxFrame2() {
        heavybox.offset = new Vector2(1.892062f, -0.1217397f);
        heavybox.size = new Vector2(5.252706f, 4.96313f);

    }

    void JumpingHeavyHitboxFrame3() {
        heavybox.offset = new Vector2(1.892062f, -0.1217397f);
        heavybox.size = new Vector2(5.252706f, 4.96313f);

    }

    void JumpingHeavyHitboxFrame4() {
        heavybox.offset = new Vector2(1.892062f, -0.1217397f);
        heavybox.size = new Vector2(5.252706f, 4.96313f);

    }

    void JumpingHeavyHitboxFrame5() {
        heavybox.offset = new Vector2(1.892062f, -0.1217397f);
        heavybox.size = new Vector2(5.252706f, 4.96313f);

    }
    #endregion


}