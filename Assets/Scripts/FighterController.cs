﻿using UnityEngine;
using System.Collections;

public class FighterController : MonoBehaviour {

    private float maxHealth = 100;
    public float currHealth = 75; //public for testing purposes only
    public float maxSpeed;
    public bool facingRight;
    protected Animator anim;

    Rigidbody2D player;
    public KeyCode jump, calmTaunt, crouch;
    public NormalsObject normals;
    public KeyCode pummel, flipper, breakMove, bustah;

    bool grounded = false;
    bool crouching = false;
    public Transform groundCheck;
    float groundRadius = 0.2f;
    float groundOrAir = 0.0f;
    public LayerMask whatIsGround;

    public float jumpForce = 500.0f;
    float move;

    public GameObject opponent;
    bool doingSomething = false;
    public HitboxManager hitManager;

    FrameCounter hitStunTimer = new FrameCounter();

    
    // Use this for initialization
    void Start() {
        anim = GetComponent<Animator>();
        player = gameObject.GetComponent<Rigidbody2D>();
        anim.SetBool("calmTaunt", true);
    }

    // Update is called once per frame
    protected virtual void FixedUpdate() {
        if (hitStunTimer.IsPlaying())
            hitStunTimer.UpdateTimer();
        else
        {
            if (player.tag == "Player 2")
            {
                move = Input.GetAxis("Player 2");
                groundOrAir = Input.GetAxis("Vertical 2");
                if (move > 0)
                {
                    if (facingRight)
                    {
                        anim.SetBool("FrontBack", true);
                    }
                    else
                    {
                        anim.SetBool("FrontBack", false);
                    }
                }
                else
                {
                    if (!facingRight)
                    {
                        anim.SetBool("FrontBack", true);
                    }
                    else
                    {
                        anim.SetBool("FrontBack", false);
                    }
                }
            }

            if (player.tag == "Player")
            {
                move = Input.GetAxis("Player 1");
                groundOrAir = Input.GetAxis("Vertical");
                if (move < 0)
                {
                    if (facingRight)
                    {
                        anim.SetBool("FrontBack", false);
                    }
                    else
                    {
                        anim.SetBool("FrontBack", true);
                    }
                }
                else
                {
                    if (!facingRight)
                    {
                        anim.SetBool("FrontBack", false);
                    }
                    else
                    {
                        anim.SetBool("FrontBack", true);
                    }
                }
            }

            if (facingRight)
            {
                anim.SetBool("Block", !anim.GetBool("FrontBack"));
            }
            else
            {
                anim.SetBool("Block", !anim.GetBool("FrontBack"));
            }

            if (anim.GetFloat("Crouch") == 1.0f)
            {
                doingSomething = true;
            }
            else
            {
                doingSomething = false;
            }

            if (grounded && !doingSomething)
                player.velocity = new Vector2(move * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);

            if (groundOrAir < 0)
            {
                anim.SetFloat("Crouch", Mathf.Abs(groundOrAir));
            }
            else
            {
                anim.SetFloat("Speed", Mathf.Abs(move));
            }

            CheckFunctions();

            if (grounded)
            {
                if (AngleDir(transform.position, opponent.transform.position) > 0.0 && facingRight)
                {
                    Flip();
                }
                else if (AngleDir(transform.position, opponent.transform.position) < 0.0 && !facingRight)
                {
                    Flip();
                }
            }
        }
    }

    void CheckFunctions() {

        Crouch();
        Jump();

        if (Input.GetKeyDown(normals.Punch)) {
            anim.SetTrigger("Punch");
        } else if (Input.GetKeyDown(normals.Kick)) {
            anim.SetTrigger("Kick");
        } else if (Input.GetKeyDown(normals.Medium)) {
            anim.SetTrigger("Medium");
            hitManager.ActivateMove(60);
        } else if (Input.GetKeyDown(normals.Heavy)) {
            anim.SetTrigger("Heavy");
        } else if (Input.GetKeyDown(bustah)) {
            anim.SetTrigger("Bustah");
        }

        Pummel();
        Flipper();
        Break();

        Taunt();
    }

    void Flip() {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void Crouch() {
        if (anim.GetFloat("Crouch") == 1.0) {
            crouching = true;
        } else {
            crouching = false;
        }
    }

    void Pummel() {
        if (this.anim.GetCurrentAnimatorStateInfo(0).IsName("Pummel")) {
            anim.SetBool("Pummel", false);
        }

        if (Input.GetKeyDown(pummel) && grounded) {
            anim.SetBool("Pummel", true);
        }
    }

    void Flipper() {
        if (this.anim.GetCurrentAnimatorStateInfo(0).IsName("Flipper")) {
            anim.SetBool("Flipper", false);
        }

        if (Input.GetKeyDown(flipper) && grounded) {
            anim.SetBool("Flipper", true);
        }
    }

    void Break() {
        if (this.anim.GetCurrentAnimatorStateInfo(0).IsName("Break")) {
            anim.SetBool("Break", false);
        }

        if (Input.GetKeyDown(breakMove) && grounded) {
            anim.SetBool("Break", true);
        }
    }

    void Jump() {
        //Handles the characters jumping speed

        grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
        anim.SetBool("Grounded", grounded);
        var moving = false;
        var jumpSpeed = 0.0f;

        if (player.velocity.x != 0.0f) {
            moving = true;
            if (player.velocity.x < 20.0f) {
                jumpSpeed = -70.0f;
            } else if (player.velocity.x > -20.0f) {
                jumpSpeed = 70.0f;
            }

        }

        //if (groundOrAir > 0 && grounded) {
        //    player.AddForce(Vector2.up * jumpForce);
        //}

        if (grounded && Input.GetKeyDown(jump)) {
            player.AddForce(Vector2.up * jumpForce);
        }

        if (!grounded && moving) {
            player.velocity = new Vector2(jumpSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }

    }

    void Taunt() {
        if (this.anim.GetCurrentAnimatorStateInfo(0).IsName("Calm_Taunt")) {
            anim.SetBool("calmTaunt", false);
        }

        if (Input.GetKeyDown(calmTaunt)) {
            anim.SetBool("calmTaunt", true);
        }
    }

    public static float AngleDir(Vector2 A, Vector2 B) {
        return -A.x * B.y + A.y * B.x;
    }

    public void TakeDamage(float damage, int hitStunDuration, int hitPriority, Vector2 knockback)
    {
        Debug.Log("OUCH: " + name);
        currHealth -= damage;
        hitStunTimer.Reset();
        hitStunTimer.SetEndFrame(hitStunDuration);
        hitStunTimer.SetCallbackFunction(EndHitStun);
        hitStunTimer.Play();
        anim.SetBool("Block", true);
    }

    //This is used as a callback for hitStunTimer. This can also possibly
    //be used later for other effects that cancel hitstun?
    public void EndHitStun()
    {
        Debug.Log("Hitstun ended");
        anim.SetBool("Block", false);
        if (hitStunTimer.IsPlaying())
            hitStunTimer.Reset(); //pauses timer as well if still playing
    }

    /*void OnTriggerEnter2D(Collider2D collider) {
        Debug.Log("GET REKD");
        if (player.tag == "Player 2") {
            anim.SetBool("Collision", true);
        } else if (player.tag == "Player") {
            anim.SetBool("Collision", true);
        }
    }

    void OnTriggerExit2D(Collider2D collider) {
        Debug.Log("GET UNREKD");
        if (player.tag == "Player 2") {
            anim.SetBool("Collision", false);
        } else if (player.tag == "Player") {
            anim.SetBool("Collision", false);
        }
    }

    void OnCollisionEnter2D(Collision2D collision) {
        /*
        anim.SetBool("Collision", false);

        Debug.Log(collision.gameObject.tag);
            anim.SetBool("Collision", true);


       if (collision.gameObject.tag == "Player") {
            player.velocity = new Vector2(-300f, GetComponent<Rigidbody2D>().velocity.y);

            Debug.Log("Collided with " + collision.gameObject.name);
        }
        
    }*/

    public float GetHealthRatio()
    {
        return currHealth / maxHealth;
    }

}


