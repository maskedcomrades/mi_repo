﻿using UnityEngine;
using System.Collections;

public class PunchBoxes : MonoBehaviour {

    public BoxCollider2D punchbox;

    void PunchHitboxEnd() {
        punchbox.size = new Vector2(0.0f, 0.0f);

    }

    #region standing punch
    void PunchHitboxStart () {
        punchbox.offset = new Vector2(2.329857f, 0.9200363f);
        punchbox.size = new Vector2(2.259449f, 1.363495f);

    }

    void PunchHitboxFrame2() {
        punchbox.offset = new Vector2(2.438375f, 0.9200363f);
        punchbox.size = new Vector2(2.47648f, 1.363495f);

    }

    void PunchHitboxFrame3() {
        punchbox.offset = new Vector2(2.205839f, 0.9200363f);
        punchbox.size = new Vector2(2.011409f, 1.363495f);

    }

    #endregion

    #region crounching punch
    void CrouchingPunchHitboxStart() {
        punchbox.offset = new Vector2(2.01628f, -0.473526f);
        punchbox.size = new Vector2(1.85939f, 1.36291f);

    }

    void CrouchingPunchHitboxFrame2() {
        punchbox.offset = new Vector2(2.099045f, -0.473526f);
        punchbox.size = new Vector2(2.02492f, 1.36291f);

    }

    void CrouchingPunchHitboxFrame3() {
        punchbox.offset = new Vector2(1.974895f, -0.473526f);
        punchbox.size = new Vector2(1.776622f, 1.36291f);

    }

    #endregion

    #region jumping punch
    void JumpingPunchHitboxStart() {
        punchbox.offset = new Vector2(1.209291f, 2.278445f);
        punchbox.size = new Vector2(2.314573f, 4.135478f);

    }

    void JumpingPunchHitboxFrame2() {
        punchbox.offset = new Vector2(1.209291f, 2.278445f);
        punchbox.size = new Vector2(2.314573f, 4.135478f);

    }

    void JumpingPunchHitboxFrame3() {
        punchbox.offset = new Vector2(1.209291f, 2.278445f);
        punchbox.size = new Vector2(2.314573f, 4.135478f);

    }

    void JumpingPunchHitboxFrame4() {
        punchbox.offset = new Vector2(1.209291f, 2.278445f);
        punchbox.size = new Vector2(2.314573f, 4.135478f);

    }
    #endregion
}
