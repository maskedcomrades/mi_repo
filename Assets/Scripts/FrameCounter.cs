﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Rather than relying on complicated second-based timers provided in C#, we can count
/// frames, since most, if not all, events are counted via frames in fighting games.
/// </summary>
public class FrameCounter : Object {
    int currFrameNumber;
    int finalFrameNumber;
    bool paused = true; //starts paused
    bool complete = false;
    bool callbackSet = false;
    public bool looping = false;

    //function pointer type declaration (void function, no arguments)
    public delegate void CallbackFunction();

    //stored function pointer
    CallbackFunction callback;

    // Every owner of this object should be calling this in FixedUpdate
    public void UpdateTimer() {
        if (!paused && !complete)
        {
            currFrameNumber++;
            if (currFrameNumber >= finalFrameNumber)
            {
                complete = true;
                if (callbackSet) callback();
                if (looping) Reset(false); //if looping, reset and start again
            }
        }
	}

    public void SetEndFrame(int frameNumber)
    {
        if (frameNumber > 0)
            finalFrameNumber = frameNumber;
    }

    public void SetCallbackFunction(CallbackFunction myCallback)
    {
        callback = myCallback;
        callbackSet = true;
    }

    public float TimeRatio()
    {
        float ratio = currFrameNumber / finalFrameNumber;
        if (ratio > 1.0)
            ratio = 1.0f;
        return ratio;
    }

    public bool IsComplete()
    {
        return complete;
    }

    public bool IsPlaying()
    {
        return (!paused && !complete);
    }

    public bool IsPaused()
    {
        return (paused || complete);
    }

    public void Play()
    {
        paused = false;
    }

    public void Pause()
    {
        paused = true;
    }

    public void Reset(bool startPaused = true)
    {
        currFrameNumber = 0;
        complete = false;
        paused = startPaused;
    }
}
